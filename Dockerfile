FROM fedora:latest
MAINTAINER Nathan Valentine <nrvale00@gmail.com>
LABEL vendor="Nathan Valentine" \
      email="nrvale00@gmail.com" \
      repo="https://gitlab.com/nrvale0/docker-clusterdebug"

ENV PATH /bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin

RUN dnf upgrade -y && \
    dnf install -y \
        net-tools \
	expect \
    	jq \
	which \
    	nmap \
	openssl \
	less \
	bmon \
	puppet \
	httpie \
	git \
	socat \
	nmap-ncat \
	wireshark-cli \
	htop \
	lsof \
	curl \
	wget \
	links \
	postgresql \
	mysql \
	screen \
	tmux \
        unzip \
        httpie \
        rubygem-mustache \
	bind-utils \
        mtr \
	tree && \
	dnf clean all -y && \
	rm -rf /tmp/*

RUN dnf install -y dnf-plugins-core && \
	dnf config-manager --add-repo https://rpm.releases.hashicorp.com/fedora/hashicorp.repo && \
        dnf install -y consul
        
COPY Dockerfile /Dockerfile
